e n .  
 A U _ I N S T A L L A T I O N _ I N _ P R O G R E S S _ M E S S A G E = { T A R G E T _ A P P _ T E X T }   i n s t a l l e r a s .   L � t   d e n   h � r   u p p d a t e r i n g e n   g �   f � r d i g t   i n n a n   d u   a v s l u t a r .  
 A U _ R E B O O T _ B E F O R E _ I N S T A L L _ M E S S A G E = D e t   f i n n s   u p p d a t e r i n g a r   a t t   h � m t a   s o m   k a n   o r s a k a   o m s t a r t   a v   d a t o r n .   S p a r a   v a d   d u   a r b e t a r   m e d   i n n a n   d u   f o r t s � t t e r   m e d   i n s t a l l a t i o n e n .   K l i c k a   p �   I n s t a l l e r a   n u ,   s �   f o r t s � t t e r   i n s t a l l a t i o n e n .  
 A U _ R E B O O T _ A F T E R _ I N S T A L L _ M E S S A G E = S t a r t a   o m   d a t o r n   s �   t i l l � m p a s   u p p d a t e r i n g a r n a .   V i l l   d u   s t a r t a   o m   d a t o r n   n u ?  
 A U _ I N S T A L L _ N O W _ T E X T = I n s t a l l e r a   n u  
 A U _ I N S T A L L _ L A T E R _ T E X T = I n s t a l l e r a   s e n a r e  
  
 # W I N 8   O O B E  
 C R I T I C A L _ U P D A T E _ T O A S T _ T E X T = D u   h a r   e n   e l l e r   f l e r a   k r i t i s k a   u p p d a t e r i n g a r   t i l l g � n g l i g a   f � r   d i t t   s y s t e m .  
 C R I T I C A L _ U P D A T E _ M E S S A G E 1 _ T E X T = D u   h a r   k r i t i s k a   u p p d a t e r i n g a r   t i l l g � n g l i g a   f � r   d i t t   s y s t e m .   L e n o v o   r e k o m m e n d e r a r   a t t   d u   i n s t a l l e r a r   d e m   n u   f � r   a t t   o p t i m e r a   d i n   d a t o r .   M i n s t   e n   a v   u p p d a t e r i n g a r n a   k a n   s t a r t a   o m   d i t t   s y s t e m   s �   s p a r a   a l l t   a r b e t e   i n n a n   d u   f o r t s � t t e r .  
 C R I T I C A L _ U P D