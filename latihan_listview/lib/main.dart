import 'package:flutter/material.dart';

void main() {
  runApp(ListViewPage());
}

class ListViewPage extends StatelessWidget {
  var negara = [
    'Inggris',
    'Italia',
    'Israel',
    'Palestina',
    'Vatikan',
    'Belanda',
    'Argentina',
    'Jepang',
    'Korea Utara',
    'Korea Selatan',
    'Portugal',
    'Malaysia',
    'Indonesia',
    'Filipina',
    'Thailand',
    'Amerika Serikat',
    'Kamboja',
    'China'
  ];
  @override
  Widget build(BuildContext context) {
    /*return MaterialApp(
      home: Scaffold(
        body: ListView(
          children: [
            for (var i = 1; i < negara.length; i++)
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(bottom: 5),
                decoration: BoxDecoration(border: Border.all(width: 1)),
                child: Text(negara[i]),
              ),
          ],
        ),
      ),
    );
  }*/
    return MaterialApp(
      home: Scaffold(
        body: ListView.builder(
            padding: const EdgeInsets.all(8),
            itemCount: negara.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                color: index % 2 == 0 ? Colors.blue : Colors.red,
                height: 50,
                child: Center(child: Text('${negara[index]}')),
              );
            }),
      ),
    );
  }
}
