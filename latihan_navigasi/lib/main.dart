import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:latihan_navigasi/contact.dart';
import 'package:latihan_navigasi/home.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MainPage(),
  ));
}

class MainPage extends StatelessWidget {
  final _pageController = PageController();

  @override
  void dispose() {
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
          /*ElevatedButton(
          child: Container(child: Image.asset('images/google.webp')),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Screen2(gambar: 'facebook.png')));
          },
        ),
        ElevatedButton(
          child: Container(child: Image.asset('images/facebook.png')),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Screen2(gambar: 'facebook.png')));
          },
        ),*/
          PageView(
        controller: _pageController,
        children: [HomePage(), ContactPage()],
      ),
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Colors.blue,
        buttonBackgroundColor: Colors.white,
        height: 65,
        items: const <Widget>[
          Icon(
            Icons.home,
            size: 35,
          ),
          Icon(
            Icons.person,
            size: 35,
          ),
        ],
        onTap: (index) {
          _pageController.animateToPage(index,
              duration: const Duration(milliseconds: 300),
              curve: Curves.easeOut);
        },
      ),
    );
  }
}
