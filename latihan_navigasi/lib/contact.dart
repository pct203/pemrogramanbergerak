import 'package:flutter/material.dart';
import 'package:latihan_navigasi/detail_contact.dart';

class ContactPage extends StatelessWidget {
  var mahasiswa = [
    {
      'npm': '021210026',
      'nama': 'Aisah',
      'jk': 'P',
      'prodi': 'Sistem Informasi Program Sarjana',
      'gambar': 'images/aisah.png'
    },
    {
      'npm': '011210046',
      'nama': 'Femas Kurniawan',
      'jk': 'L',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '011210001',
      'nama': 'Fernando',
      'jk': 'L',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '011210012',
      'nama': 'Frima Mandala Putra',
      'jk': 'L',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '021210011',
      'nama': 'Ingrid Indriani',
      'jk': 'P',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
    {
      'npm': '011210040',
      'nama': 'Intan Seliandika',
      'jk': 'P',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '021210003',
      'nama': 'Kautsar Hidayatullah',
      'jk': 'L',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
    {
      'npm': '021210068',
      'nama': 'Leony Savayona',
      'jk': 'P',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
    {
      'npm': '021210010',
      'nama': 'Miranda Rahma Puspita Sari',
      'jk': 'P',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
    {
      'npm': '011210042',
      'nama': 'Muhamad Gymnastiar',
      'jk': 'L',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '011210035',
      'nama': 'Muhammad Alif Al Fajra',
      'jk': 'L',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '011210022',
      'nama': 'Muhammad Fajar Ikhwan',
      'jk': 'L',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '021210072',
      'nama': 'Muhammad Fajri Al Majid',
      'jk': 'L',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
    {
      'npm': '021210045',
      'nama': 'Nurjulianti',
      'jk': 'P',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
    {
      'npm': '011210005',
      'nama': 'Okta Pitriani',
      'jk': 'P',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '011210009',
      'nama': 'Prayoga Kurniawan',
      'jk': 'L',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '011210023',
      'nama': 'Putra Wira Albarokah',
      'jk': 'L',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '011210043',
      'nama': 'Rafly Dief Setiawan',
      'jk': 'L',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '021210042',
      'nama': 'Rahman Ivan Nasikin',
      'jk': 'L',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
    {
      'npm': '021210061',
      'nama': 'Rani Wiranda',
      'jk': 'P',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
    {
      'npm': '011210024',
      'nama': 'Rily Dwi Marsela',
      'jk': 'P',
      'prodi': 'Informatika Program Sarjana'
    },
    {
      'npm': '021210073',
      'nama': 'Rini A\'yuni',
      'jk': 'P',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
    {
      'npm': '021210084',
      'nama': 'Rizqi Irawan',
      'jk': 'L',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
    {
      'npm': '021210023',
      'nama': 'Supar Wadi',
      'jk': 'L',
      'prodi': 'Sistem Informasi Program Sarjana'
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          for (int i = 0; i < mahasiswa.length; i++)
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailContactPage(
                            nama: mahasiswa[i]['nama'].toString(),
                            npm: mahasiswa[i]['npm'].toString(),
                            jk: mahasiswa[i]['jk'].toString(),
                            prodi: mahasiswa[i]['prodi'].toString())));
              },
              child: Container(
                  height: 50,
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: Colors.grey, width: 1))),
                  child: Row(children: [
                    Icon(
                      Icons.person,
                      color:
                          mahasiswa[i]['jk'] == 'P' ? Colors.pink : Colors.blue,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(mahasiswa[i]['nama'].toString()),
                  ])),
            )
        ],
      ),
    );
  }
}
